def entrar()
  $driver.wait_true (5) {$driver.exists {$driver.find_elements(:id, "text1")[2].click } }
end

def sair()
  sleep 5
  $driver.wait_true (5) {$driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/action_navigation_profile'} }
  perfil = $driver.find_element :id, 'co.socialsquad.squad.debug:id/action_navigation_profile'
  perfil.click
  $driver.wait_true (5) {$driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/profile_mi_settings'} }
  config = $driver.find_element :id, 'co.socialsquad.squad.debug:id/profile_mi_settings'
  config.click
  $driver.wait_true (3) {$driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/bLogout'} }
  sair = $driver.find_element :id, 'co.socialsquad.squad.debug:id/bLogout'
  sair.click
  sleep 5
end

def logar()
  $driver.wait_true (5){$driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/walkthrough_b_login'} }
  entrar = $driver.find_element :id, 'co.socialsquad.squad.debug:id/walkthrough_b_login'
  entrar.click

  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tietEmail'} }
  email = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tietEmail'
  email.send_key "vanessa@squad.com.br"

  $driver.wait_true (2) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_next'} }
  proximo = $driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_next'
  proximo.click

  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tietPassword'} }
  senha =  $driver.find_element :id, 'co.socialsquad.squad.debug:id/tietPassword'
  senha.send_key "abc123"

  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_done'} }
  ok = $driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_done'
  ok.click
end



