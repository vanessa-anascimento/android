# language: pt

@drops
Funcionalidade: Drops


Cenario: Gravar novo Drop pelo App

Dado que eu queira criar um novo Drop	
Quando acessar a tela e clicar em Novo Drop
E começar a gravar
Então quando finalizar, escrevendo titulo e salvar, o drop deverá estar disponíveis

Cenario: Drop sem Titulo

Dado que eu grave um drop
Quando for salvar e deixar o titulo em branco
Então um erro será exibido 

Cenario: Drop sem descrição

Dado que eu tenha gravado um drop
Quando for salvar e deixar a descriçao em branco
Então o drop deverá ser salvo

Cenario: Regravar Drop

Dado que eu esteja gravando um audio
Quando terminar a gravaçao e clicar em regravar
Então o audio deverá ser regravado com sucesso

Cenario: Escolher drop na galeria

Dado que eu queira criar um drop novo
Quando acessar a tela, clicar em Novo Drop
E escolher a opçao Galeria
Então quando finalizar, o drop será salvo com sucesso

#Cenario: Deletar drop
#Dado que tenha drops na minha lista
#Quando clicar em deletar drop nas opções
#Então o drop deverá ser deletado

Cenário: Compartilhar drop
Dado que eu tenha drops públicos
Quando clicar em compartilhar
Então o drop deverá ser compartilhado com a rede social escolhida

Cenario: Compartilhar drop não público
Dado que eu tenha drops não públicos
Quando tentar compartilhar
Então não deverá ser exibido o ícone de compartilhamento
