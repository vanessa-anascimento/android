# language: pt

Funcionalidade: Logar app

Cenario: Logar com usuário válido 

Dado que tenha um usuário válido
Quando eu digitar o usuário e a senha
Entao o aplicativo irá logar
   

Cenario: Logar com senha inválida

Dado que tenha um usuário	
Quando o usuário digitar o usuario e uma senha inválida
Entao o aplicativo irá exibir um erro 

Cenario: Logar com usuario inválido

Dado que tenha um usuário inválido
Quando o usuário digitar a senha
Entao um erro será exibido

Cenario: Logar com campos em branco

Dado que tenha um usuario
Quando o usuario deixar os campos de usuario em senha em branco
Entao o botão proximo, não será habilitado

Cenario: Logar com usuario em branco

Dado que um usuario tente se logar
Quando eu deixar o campo usuario em branco e digitar a senha
Entao o botão proximo, não será habilitado

Cenario: Logar com senha em branco

Dado que eu tenha um usuario para logar
Quando eu digitar o usuario e deixar a senha em branco
Entao o botão proximo, não deve estar habilitado

    