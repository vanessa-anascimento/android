
Dado("que eu queira criar um novo Drop") do
   entrar
   logar

   $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/ivDropIndicator'} }
   drop = $driver.find_element :id, 'co.socialsquad.squad.debug:id/ivDropIndicator'
   drop.click
end

Quando("acessar a tela e clicar em Novo Drop") do
  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_mi_create'} }
  criar = $driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_mi_create'
  criar.click

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tvName'} }
  grav = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tvName'
  grav.click
end

Quando("começar a gravar") do
  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tvRecord'} }
  gravar = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tvRecord'
  gravar.click
  sleep 10

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/ivStop'} }
  stop = $driver.find_element :id, 'co.socialsquad.squad.debug:id/ivStop'
  stop.click

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/menu_audio_record_next'} }
  proximo = $driver.find_element :id, 'co.socialsquad.squad.debug:id/menu_audio_record_next'
  proximo.click
end

Então("quando finalizar, escrevendo titulo e salvar, o drop deverá estar disponíveis") do
  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/etInput'} }
  titulo = $driver.find_element :id, 'co.socialsquad.squad.debug:id/etInput'
  titulo.send_keys "Teste"

  hide_keyboard

  desc = $driver.find_element :id, 'co.socialsquad.squad.debug:id/itDescription'
  desc.send_keys "Texto"

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_create_mi_save'} }
  save = $driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_create_mi_save'
  save.click

  element = $driver.find_element :id,'android:id/message'
  puts element.text

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'android:id/button1'} }
  ok = $driver.find_element :id, 'android:id/button1'
  ok.click
end

####################################################################################################################################

Dado("que eu grave um drop") do
  entrar

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/ivDropIndicator'} }
  drop = $driver.find_element :id, 'co.socialsquad.squad.debug:id/ivDropIndicator'
  drop.click

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_mi_create'} }
  criar = $driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_mi_create'
  criar.click

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tvName'} }
  grav = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tvName'
  grav.click

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tvRecord'} }
  gravar = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tvRecord'
  gravar.click
  sleep 10

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/ivStop'} }
  stop = $driver.find_element :id, 'co.socialsquad.squad.debug:id/ivStop'
  stop.click

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/menu_audio_record_next'} }
  proximo = $driver.find_element :id, 'co.socialsquad.squad.debug:id/menu_audio_record_next'
  proximo.click
end

Quando("for salvar e deixar o titulo em branco") do
  hide_keyboard

  desc = $driver.find_element :id, 'co.socialsquad.squad.debug:id/itDescription'
  desc.send_keys "Texto"

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_create_mi_save'} }
  save = $driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_create_mi_save'
  save.click
end

Então ("um erro será exibido") do 
  element = $driver.find_element :id,'android:id/message'
  puts element.text

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'android:id/button1'} }
  ok = $driver.find_element :id, 'android:id/button1'
  ok.click
end

####################################################################################################################################

Dado("que eu tenha gravado um drop") do
  entrar
  
  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/ivDropIndicator'} }
  drop = $driver.find_element :id, 'co.socialsquad.squad.debug:id/ivDropIndicator'
  drop.click

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_mi_create'} }
  criar = $driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_mi_create'
  criar.click
   
  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tvName'} }
  grav = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tvName'
  grav.click

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tvRecord'} }
  gravar = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tvRecord'
  gravar.click
  sleep 10

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/ivStop'} }
  stop = $driver.find_element :id, 'co.socialsquad.squad.debug:id/ivStop'
  stop.click

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/menu_audio_record_next'} }
  proximo = $driver.find_element :id, 'co.socialsquad.squad.debug:id/menu_audio_record_next'
  proximo.click
end

Quando("for salvar e deixar a descriçao em branco") do
  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/etInput'} }
  titulo = $driver.find_element :id, 'co.socialsquad.squad.debug:id/etInput'
  titulo.send_keys "Teste"

  hide_keyboard

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_create_mi_save'} }
  save = $driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_create_mi_save'
  save.click
end

Então("o drop deverá ser salvo") do
  element = $driver.find_element :id,'android:id/message'
  puts element.text

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'android:id/button1'} }
  ok = $driver.find_element :id, 'android:id/button1'
  ok.click
end

####################################################################################################################################

Dado("que eu esteja gravando um audio") do
  entrar

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/ivDropIndicator'} }
  drop = $driver.find_element :id, 'co.socialsquad.squad.debug:id/ivDropIndicator'
  drop.click

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_mi_create'} }
  criar = $driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_mi_create'
  criar.click
 
  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tvName'} }
  grav = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tvName'
  grav.click

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tvRecord'} }
  gravar = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tvRecord'
  gravar.click
  sleep 10

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/ivStop'} }
  stop = $driver.find_element :id, 'co.socialsquad.squad.debug:id/ivStop'
  stop.click
end

Quando("terminar a gravaçao e clicar em regravar") do
  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tvRecordAgain'} }
  regravar = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tvRecordAgain'
  regravar.click

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tvRecord'} }
  gravar = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tvRecord'
  gravar.click
  sleep 10
 
  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/ivStop'} }
  stop = $driver.find_element :id, 'co.socialsquad.squad.debug:id/ivStop'
  stop.click

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/menu_audio_record_next'} }
  proximo = $driver.find_element :id, 'co.socialsquad.squad.debug:id/menu_audio_record_next'
  proximo.click
 end

Então("o audio deverá ser regravado com sucesso") do
  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/etInput'} }
  titulo = $driver.find_element :id, 'co.socialsquad.squad.debug:id/etInput'
  titulo.send_keys "Teste"

  hide_keyboard

  desc = $driver.find_element :id, 'co.socialsquad.squad.debug:id/itDescription'
  desc.send_keys "Texto"

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_create_mi_save'} }
  save = $driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_create_mi_save'
  save.click

  element = $driver.find_element :id,'android:id/message'
  puts element.text

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'android:id/button1'} }
  ok = $driver.find_element :id, 'android:id/button1'
  ok.click
end

####################################################################################################################################

Dado("que eu queira criar um drop novo") do
  entrar

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/ivDropIndicator'} }
  drop = $driver.find_element :id, 'co.socialsquad.squad.debug:id/ivDropIndicator'
  drop.click
end

Quando("acessar a tela, clicar em Novo Drop") do
  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_mi_create'} }
  criar = $driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_mi_create'
  criar.click
end

Quando("escolher a opçao Galeria") do
  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'audio_create_dialog_button_gallery'} }
  galeria = $driver.find_element :id, 'audio_create_dialog_button_gallery'
  galeria.click

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'android:id/title'} }
  gravacao = $driver.find_element :id, 'android:id/title'
  gravacao.click
end

Então("quando finalizar, o drop será salvo com sucesso") do
  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/etInput'} }
  titulo = $driver.find_element :id, 'co.socialsquad.squad.debug:id/etInput'
  titulo.send_keys "Teste"

  hide_keyboard

  desc = $driver.find_element :id, 'co.socialsquad.squad.debug:id/itDescription'
  desc.send_keys "Texto"

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_create_mi_save'} }
  save = $driver.find_element :id, 'co.socialsquad.squad.debug:id/audio_create_mi_save'
  save.click

  element = $driver.find_element :id,'android:id/message'
  puts element.text

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'android:id/button1'} }
  ok = $driver.find_element :id, 'android:id/button1'
  ok.click
end

####################################################################################################################################

Dado("que tenha drops na minha lista") do
  entrar

  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/ivDropIndicator'} }
  drop = $driver.find_element :id, 'co.socialsquad.squad.debug:id/ivDropIndicator'
  drop.click
end

Quando("clicar em deletar drop nas opções") do
  sleep 5
  $driver.wait_true (20) { $driver.exists {$drive.find_element :id, 'co.socialsquad.squad.debug:id/sOptions'} }
  opts = $drive.find_element :id, 'co.socialsquad.squad.debug:id/sOptions'
  opts.click 

  sleep 3
  $driver.find_elements(:id, "audio_option_delete")[0].click

  element = $driver.find_element :id,'android:id/message'
  puts element.text
end

Então("o drop deverá ser deletado") do
  $driver.wait_true (6) { $driver.exists {$driver.find_element :id, 'android:id/button1'} }
  sim = $driver.find_element :id, 'android:id/button1'
  sim.click
  sleep 3
end

####################################################################################################################################

Dado("que eu tenha drops públicos") do
  pending # Write code here that turns the phrase above into concrete actions
end

Quando("clicar em compartilhar") do
  pending # Write code here that turns the phrase above into concrete actions
end

Então("o drop deverá ser compartilhado com a rede social escolhida") do
  pending # Write code here that turns the phrase above into concrete actions
end

####################################################################################################################################

Dado("que eu tenha drops não públicos") do
  pending # Write code here that turns the phrase above into concrete actions
end

Quando("tentar compartilhar") do
  pending # Write code here that turns the phrase above into concrete actions
end

Então("não deverá ser exibido o ícone de compartilhamento") do
  pending # Write code here that turns the phrase above into concrete actions
end