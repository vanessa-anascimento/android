
Dado("que tenha um usuário válido") do
  entrar
end

Quando("eu digitar o usuário e a senha") do
  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tietEmail'} }
  email = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tietEmail'
  email.send_key "vanessa@squad.com.br"

  $driver.wait_true (2) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_next'} }
  proximo = $driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_next'
  proximo.click

  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tietPassword'} }
  senha =  $driver.find_element :id, 'co.socialsquad.squad.debug:id/tietPassword'
  senha.send_key "abc123"

  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_done'} }
  ok = $driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_done'
  ok.click

end

Entao("o aplicativo irá logar") do
  sair
end

###########################################################################################################################################

Dado("que tenha um usuário") do
 entrar
end

Quando("o usuário digitar o usuario e uma senha inválida") do
  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tietEmail'} }
  email = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tietEmail'
  email.send_key "vanessa@squad.com.br"

  $driver.wait_true (2) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_next'} }
  proximo = $driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_next'
  proximo.click

  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tietPassword'} }
  senha =  $driver.find_element :id, 'co.socialsquad.squad.debug:id/tietPassword'
  senha.send_key "123456"

  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_done'} }
  ok = $driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_done'
  ok.click
end

Entao("o aplicativo irá exibir um erro") do
sleep 10

element = $driver.find_element :id, 'android:id/message'
puts element.text

end

###########################################################################################################################################

Dado("que tenha um usuário inválido") do
  entrar
end

Quando("o usuário digitar a senha") do
  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tietEmail'} }
  email = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tietEmail'
  email.send_key "vanessa@vanessa.com.br"

  $driver.wait_true (2) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_next'} }
  proximo = $driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_next'
  proximo.click

  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tietPassword'} }
  senha =  $driver.find_element :id, 'co.socialsquad.squad.debug:id/tietPassword'
  senha.send_key "abc123"

  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_done'} }
  ok = $driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_done'
  ok.click
end

Entao("será exibido erro") do

sleep 10

element = $driver.find_element :id, 'android:id/message'
puts element.text

end

###########################################################################################################################################

Dado("que tenha um usuario") do
  entrar
end

Quando("o usuario deixar os campos de usuario em senha em branco") do
  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tietEmail'} }
  email = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tietEmail'
  email.send_key ""

  $driver.wait_true (2) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_next'} }
  proximo = $driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_next'
  proximo.click

  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tietPassword'} }
  senha =  $driver.find_element :id, 'co.socialsquad.squad.debug:id/tietPassword'
  senha.send_key ""

  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_done'} }
  ok = $driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_done'
  ok.click
end

Entao("o botão proximo, não é habilitado") do
  pending # Write code here that turns the phrase above into concrete actions
end

###########################################################################################################################################

Dado("que um usuario tente se logar") do
  entrar
end

Quando("eu deixar o campo usuario em branco e digitar a senha") do
  driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tietEmail'} }
  email = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tietEmail'
  email.send_key ""

  $driver.wait_true (2) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_next'} }
  proximo = $driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_next'
  proximo.click
  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tietPassword'} }
  senha =  $driver.find_element :id, 'co.socialsquad.squad.debug:id/tietPassword'
  senha.send_key "abc123"

  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_done'} }
  ok = $driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_done'
  ok.click
end

Entao("o botão proximo, não será habilitado") do
  pending # Write code here that turns the phrase above into concrete actions
end

###########################################################################################################################################

Dado("que eu tenha um usuario para logar") do
  entrar

  $driver.wait_true (5) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/tietEmail'} }
  email = $driver.find_element :id, 'co.socialsquad.squad.debug:id/tietEmail'
  email.send_key "vanessa@squad.com.br"

  $driver.wait_true (2) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_next'} }
  proximo = $driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_next'
  proximo.click
end

Quando("eu digitar o usuario e deixar a senha em branco") do
  $driver.wait_true (2) { $driver.exists {$driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_done'} }
  ok = $driver.find_element :id, 'co.socialsquad.squad.debug:id/login_credentials_b_done'
  ok.click

end

Entao("o botão proximo, não deve estar habilitado") do
 pending # Write code here that turns the phrase above into concrete actions
end